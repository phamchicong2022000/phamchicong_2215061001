﻿using Lab4_01.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4_01
{
    public partial class Tim : Form
    {
        private List<Student> listStudent;
        public Tim()
        {
            InitializeComponent();
        }

        private void Tim_Load(object sender, EventArgs e)

        {
            LoadStudentData();
            cbchuyennganh.SelectedIndex = -1;

        }
        private void LoadStudentData()
        {
            try
            {
                Model1 context = new Model1();
                listStudent = context.Students.ToList();
                List<Faculty> listFalcultys = context.Faculties.ToList();
                FillFalcultyCombobox(listFalcultys);
                // Gán dữ liệu cho biến ở mức lớp
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void FillFalcultyCombobox(List<Faculty> listFalcultys)
        {
            this.cbchuyennganh.DataSource = listFalcultys;
            this.cbchuyennganh.DisplayMember = "FacultyName";
            this.cbchuyennganh.ValueMember = "FacultyID";
        }
        public List<Student> FindByName(String keyword)
        {
            List<Student> searchResult = new List<Student>();
            if (listStudent != null && listStudent.Count > 0)
            {
                foreach (Student sv in listStudent)
                {
                    if (sv.FullName.ToUpper().Contains(keyword.ToUpper()))
                    {
                        searchResult.Add(sv);
                    }
                }
            }
            return searchResult;
        }
        private bool rb()
        {
            if (txthovaten.Text == "" || txtmsv.Text == "" || cbchuyennganh.Text == "")
            {
                MessageBox.Show("nhap day du thong tin");
                return false;
            }
         
            return true;
        }
        private void btntim_Click(object sender, EventArgs e)
        {
            string keyword = txthovaten.Text;
            string khoa = cbchuyennganh.Text;
            string msv = txtmsv.Text;
            if (rb() == false)
                return;
            List<Student> searchResult = new List<Student>();
            try
            {
                if (!string.IsNullOrEmpty(keyword) && !string.IsNullOrEmpty(khoa) && !string.IsNullOrEmpty(msv))
                {
                    // Tìm kiếm theo cả tên và khoa
                    searchResult = listStudent
                        .Where(sv => sv.FullName.ToUpper().Contains(keyword.ToUpper()) &&
                                     sv.Faculty.FacultyName.ToUpper().Contains(khoa.ToUpper()) && sv.StudentID.ToUpper().Contains(msv.ToUpper()))
                        .ToList();
                }
                else if (!string.IsNullOrEmpty(keyword))
                {
                    // Tìm kiếm theo tên
                    searchResult = listStudent
                        .Where(sv => sv.FullName.ToUpper().Contains(keyword.ToUpper()))
                        .ToList();
                }
                else if (!string.IsNullOrEmpty(khoa))
                {
                    // Tìm kiếm theo khoa
                    searchResult = listStudent
                        .Where(sv => sv.Faculty.FacultyName.ToUpper().Contains(khoa.ToUpper()))
                        .ToList();
                }
                else if (!string.IsNullOrEmpty(msv))
                {
                    // Tìm kiếm theo khoa
                    searchResult = listStudent
                        .Where(sv => sv.StudentID.ToUpper().Contains(msv.ToUpper()))
                        .ToList();
                }


                if (searchResult.Count == 0)
                {
                    MessageBox.Show(this, "Không tìm thấy kết quả phù hợp.", "Thông báo", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    // Hiển thị kết quả trong DataGridView
                    dgsvt.DataSource = searchResult;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(this, ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txthovaten.Clear();
            txtmsv.Clear();
            cbchuyennganh.SelectedIndex = -1;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            this.Close();  
        }
    }
    }
    
        


    


