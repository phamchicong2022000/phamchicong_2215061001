﻿using Lab4_01.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4_01
{
    public partial class Form1 : Form
    {


        public Form1()
        {

            InitializeComponent();
        }
        private bool rb()
        {
            if (txthovaten.Text == "" || txtmsv.Text == "" || txtDiemTB.Text == "")
            {
                MessageBox.Show("nhap day du thong tin");
                return false;
            }
            if (txtmsv.Text.Length != 5)
            {
                MessageBox.Show("nhap lai massv phai bang 5");
                return false;
            }
            if (double.TryParse(txtDiemTB.Text, out double DTB) == false)
            {
                MessageBox.Show("nhap lai DTB la so");
                return false;
            }
            foreach (char c in txthovaten.Text)
            {
                if (char.IsDigit(c))
                {
                    MessageBox.Show("nhap lai ten");
                    return false;
                }
            }
                return true;
        }
         
        private Student TSinhVien(String MSSV)
        {
            Model1 context = new Model1();
            return context.Students.Find(MSSV);
        }

        private void btnthem_Click(object sender, EventArgs e)
        {

            if (rb() == false)
                return;
            Student Tsv = TSinhVien(txtmsv.Text);
            if(Tsv != null)
            {
                MessageBox.Show("Sinh Vien Ton Tai");
                return;
            }

            var txtMassvValue = txtmsv.Text;
            var txtTenValue = txthovaten.Text;
            var txtkhoaValue = cbchuyennganh.SelectedValue;
            var txtdiemValue = txtDiemTB.Text;

            Student st = new Student();
            st.StudentID = txtMassvValue;
            st.FullName = txtTenValue;
            st.FacultyID = (int)txtkhoaValue;
            st.AverageScore = double.Parse(txtdiemValue);
            Model1 context = new Model1();
            context.Students.Add(st);
            context.SaveChanges();
            rf();
            clear();
        }


        private void clear()
        {
            txtmsv.Clear();
            txthovaten.Clear();
            txtDiemTB.Clear();
            cbchuyennganh.SelectedIndex = 0;
        }

        private void txtmsv_TextChanged(object sender, EventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            rf();
        }
        private void rf()
        {
            try
            {
                Model1 context = new Model1();
                List<Faculty> listFalcultys = context.Faculties.ToList();
                List<Student> listStudent = context.Students.ToList();
                FillFalcultyCombobox(listFalcultys);
                BindGrid(listStudent);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
       private void FillFalcultyCombobox(List<Faculty> listFalcultys)
          {
            this.cbchuyennganh.DataSource = listFalcultys;
            this.cbchuyennganh.DisplayMember = "FacultyName";
            this.cbchuyennganh.ValueMember = "FacultyID";
          }
                 private void BindGrid(List<Student> listStudent)
                {
                    dgsv.Rows.Clear();
                    foreach (var item in listStudent)
                {
                        int index = dgsv.Rows.Add();
                        dgsv.Rows[index].Cells[0].Value = item.StudentID;
                        dgsv.Rows[index].Cells[1].Value = item.FullName;
                        dgsv.Rows[index].Cells[3].Value = item.Faculty.FacultyName;
                        dgsv.Rows[index].Cells[2].Value = item.AverageScore;
                  }
              }

        private void dgsv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btnthoat_Click(object sender, EventArgs e)
        {
            this.Close(); 
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            Model1 context = new Model1();

            if (rb() == false)
                return;
            var txtMSSVValue = txtmsv.Text;
            Student dbUpdate = context.Students.FirstOrDefault(p => p.StudentID == txtMSSVValue);
            if (dbUpdate == null)
            {
                MessageBox.Show("Khong tim thay sinh vien " + txtMSSVValue);
                return;
              }
            dbUpdate.FullName = txthovaten.Text;
            dbUpdate.FacultyID = (int)cbchuyennganh.SelectedValue;
            dbUpdate.AverageScore = double.Parse(txtDiemTB.Text);

            context.SaveChanges();
            rf();
            clear();

        }

        private void dgsv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            txtmsv.Text = dgsv.Rows[rowIndex].Cells["masv"].Value.ToString();
            txthovaten.Text = dgsv.Rows[rowIndex].Cells["hovaten"].Value.ToString();
            cbchuyennganh.Text = dgsv.Rows[rowIndex].Cells["diemtb"].Value.ToString();
            txtDiemTB.Text = dgsv.Rows[rowIndex].Cells["khoa"].Value.ToString();

        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Chắc chắn muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                Model1 context = new Model1();
                var txtMSSVValue = txtmsv.Text;
                Student dbDelete = context.Students.FirstOrDefault(p => p.StudentID == txtMSSVValue);
                if (dbDelete != null)
                {
                    context.Students.Remove(dbDelete);
                    context.SaveChanges();
                    rf();
                }
            }
           
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

   
        private void quảnLýKhoaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Form2 otherForm = new Form2();
            otherForm.ShowDialog();
        }

        private void menuToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void tìmToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Tim otherForm = new Tim();
            otherForm.ShowDialog();
        }

        private void thoátToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
    }

