﻿using Lab4_01.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab4_01
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            rf();
        }
        private void rf()
        {
            try
            {
                Model1 context = new Model1();
                List<Faculty> listFalcultys = context.Faculties.ToList();
                BindGrid(listFalcultys);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        private void BindGrid(List<Faculty> listFalcultys)
        {
            dgsv.Rows.Clear();
            foreach (var item in listFalcultys)
            {
                int index = dgsv.Rows.Add();
                dgsv.Rows[index].Cells[0].Value = item.FacultyID;
                dgsv.Rows[index].Cells[1].Value = item.FacultyName;
                dgsv.Rows[index].Cells[2].Value = item.TongGs;
            }
        }

        private bool rb()
        {
            if (txtMaKhoa.Text == "" || txtTenKhoa.Text == "" || txtGs.Text == "")
            {
                MessageBox.Show("nhap day du thong tin");
                return false;
            }
            if (txtMaKhoa.Text.Length != 5)
            {
                MessageBox.Show("nhap lai massv phai bang 5");
                return false;
            }
            if (double.TryParse(txtGs.Text, out double DTB) == false)
            {
                MessageBox.Show("nhap lai GS la so");
                return false;
            }
            foreach (char c in txtTenKhoa.Text)
            {
                if (char.IsDigit(c))
                {
                    MessageBox.Show("nhap lai ten");
                    return false;
                }
            }
            return true;
        }
        private void FillFalcultyCombobox(List<Faculty> listFalcultys)
        {
            throw new NotImplementedException();
        }
       
        private void clear()
        {
            txtMaKhoa.Clear();
            txtTenKhoa.Clear();
            txtGs.Clear();
           
        }
        private Faculty TKhoa(int FacultyID)
        {
            Model1 context = new Model1();
            return context.Faculties.Find(FacultyID);
        }
        private void btnthem_Click(object sender, EventArgs e)
        {
            if (rb() == false)
                return;
            Faculty Tsv = TKhoa(int.Parse(txtMaKhoa.Text));
            if (Tsv != null)
            {
                MessageBox.Show("Ma Khoa ton tai");
                return;
            }

            var txtMakhoaValue = txtMaKhoa.Text;
            var txtTenKhoaValue = txtTenKhoa.Text;
            var txtGsValue = txtGs.Text;

            Faculty fc = new Faculty();
            fc.FacultyID = int.Parse(txtMakhoaValue);
            fc.FacultyName = txtTenKhoaValue;
            fc.TongGs = int.Parse(txtGsValue);         
            Model1 context = new Model1();
            context.Faculties.Add(fc);
            context.SaveChanges();
            rf();
            clear();
        }

        private void btnsua_Click(object sender, EventArgs e)
        {
            Model1 context = new Model1();

            if (rb() == false)
                return;
            var txtMSKhoaValue = int.Parse(txtMaKhoa.Text);
            Faculty dbUpdate = context.Faculties.FirstOrDefault(p => p.FacultyID == txtMSKhoaValue);
            if (dbUpdate == null)
            {
                MessageBox.Show("Khong tim thay sinh vien " + txtMSKhoaValue);
                return;
            }
            dbUpdate.FacultyID = int.Parse(txtMaKhoa.Text);
            dbUpdate.FacultyName = txtTenKhoa.Text;
            dbUpdate.TongGs = int.Parse(txtGs.Text); 

            context.SaveChanges();
            rf();
            clear();

        }

        private void dgsv_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int rowIndex = e.RowIndex;
            txtMaKhoa.Text = dgsv.Rows[rowIndex].Cells["makhoa"].Value.ToString();
            txtTenKhoa.Text = dgsv.Rows[rowIndex].Cells["tenkhoa"].Value.ToString();
            txtGs.Text = dgsv.Rows[rowIndex].Cells["tonggs"].Value.ToString();
        }

        private void btnxoa_Click(object sender, EventArgs e)
        {
            DialogResult result = MessageBox.Show("Chắc chắn muốn xóa không?", "Thông báo", MessageBoxButtons.OKCancel);
            if (result == DialogResult.OK)
            {
                Model1 context = new Model1();
                var txtMSkhoaalue = int.Parse(txtMaKhoa.Text);
                Faculty dbDelete = context.Faculties.FirstOrDefault(p => p.FacultyID == txtMSkhoaalue);
                if (dbDelete != null)
                {
                    context.Faculties.Remove(dbDelete);
                    context.SaveChanges();
                    rf();
                }
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {

            this.Close();


        }
    }
}

